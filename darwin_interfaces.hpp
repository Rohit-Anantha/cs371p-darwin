# include <vector>

using namespace std;


enum instruction {
    HOP,
    LEFT,
    RIGHT,
    INFECT,
    IFEMPTY,
    IFWALL,
    IFRANDOM,
    IFENEMY,
    GO
};

enum direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
};

class species{
    public:
        vector<pair<instruction, unsigned>> instructions;
};

class creature{
    public:
        direction d;
        unsigned program_counter;
        species *s;
};

class darwin_world{
    public:
        vector<species> v_species;
        vector<creature> v_creatures;
        vector<vector<creature*>> world;
};