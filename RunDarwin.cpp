
#include <darwin_interfaces.hpp>

int main() {

    Species food;
    food.add_instruction(Instruction(LEFT));
    food.add_instruction(Instruction(GO, 0));

    // directions: 0 = right, 1 = down, 2 = left, 3 = up

    Creature c1(food, 0, 0, 0); // food type, (0,0), facing right

    DarwinWorld world(8,8);

    world.add_creature(c1);

    world.print();

    world.advance();

    world.print();

    return 0;
}